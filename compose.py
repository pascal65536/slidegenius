import file_json as fj
import settings as st
import pygame as pg


class Layer:

    def __init__(self, x, y, w, h):
        self.rect = pg.Rect(x, y, w, h)
        self.rect.x = x
        self.rect.y = y
        self.rect.w = w
        self.rect.h = h
        self.selected = False

    def move(self, dx, dy):
        self.rect.x += dx
        self.rect.y += dy

    def select(self):
        self.selected = True

    def deselect(self):
        self.selected = False

    def is_selected(self, coords):
        if not (self.rect.x <= coords[0] <= (self.rect.x + self.rect.w)):
            return None
        if not (self.rect.y <= coords[1] <= (self.rect.y + self.rect.h)):
            return None
        return self

    def __repr__(self):
        return f"{self.__class__.__name__} {self.rect=}"


class Rectangle(Layer):

    def __init__(self, name, **kwargs):
        self.name = name
        self.alpha = int(kwargs.get('alpha', 255))
        self.radius = int(kwargs.get('radius', 0))
        self.color = kwargs["color"]
        self.bordercolor = kwargs["bordercolor"]
        self.borderwidth = int(kwargs.get('borderwidth', 0))
        self.rect = pg.Rect(*kwargs["rect_lst"])
        self.selected = False

    def show(self, screen):
        shape_surf = pg.Surface(pg.Rect(self.rect).size, pg.SRCALPHA)
        shape_surf.set_alpha(int(self.alpha))
        pg.draw.rect(shape_surf,
                     self.color,
                     shape_surf.get_rect(),
                     border_radius=int(self.radius))
        screen.blit(shape_surf, self.rect)
        if int(self.borderwidth):
            shape_border = pg.Surface(pg.Rect(self.rect).size, pg.SRCALPHA)
            shape_border.set_alpha(int(self.alpha))
            pg.draw.rect(shape_border,
                        self.bordercolor,
                        shape_border.get_rect(),
                        border_radius=int(self.radius),
                        width=int(self.borderwidth))
            screen.blit(shape_border, self.rect)

        if self.selected:
            color = st.color_lst[1]
            select_border = pg.Surface(pg.Rect(self.rect).size, pg.SRCALPHA)
            select_border.set_alpha(int(self.alpha))
            pg.draw.rect(select_border,
                         color,
                         select_border.get_rect(),
                         border_radius=int(self.radius),
                         width=int(st.border_width))
            screen.blit(select_border, self.rect)

    def get_json(self):
        return {
            "name": self.name,
            "fields": {
                "obj": self.__class__.__name__,
                "color": self.color,
                "bordercolor": self.bordercolor,
                "borderwidth": int(self.borderwidth),
                "alpha": int(self.alpha),
                "radius": int(self.radius),
                "rect_lst": list(self.rect),
            },
        }


class TextBox(Layer):

    def __init__(self, name, **kwargs):
        self.name = name
        self.text = kwargs["text"]
        self.color = kwargs["color"]
        self.font_size = int(kwargs.get('font_size', 18))
        self.font_family = kwargs["font_family"]
        self.font = pg.font.Font(self.font_family, self.font_size)
        self.rect = pg.Rect(*kwargs["rect_lst"])
        self.alpha = int(kwargs.get('alpha', 255))
        self.radius = int(kwargs.get('radius', 0))
        self.selected = False

    def show(self, screen):
        text = self.text
        pos = (self.rect.x, self.rect.y)
        font = self.font
        color = self.color
        words = [word.split(" ") for word in text.splitlines()]
        space = font.size(" ")[0]
        max_width = self.rect[2]
        x, y = pos
        for line in words:
            count = 1
            for word in line:
                word_surface = font.render(word, True, color)
                word_width, word_height = word_surface.get_size()
                if x + word_width > max_width + pos[0]:
                    count += 1
                    x = pos[0]
                    y += word_height
                screen.blit(word_surface, (x, y))
                x += word_width + space
            x = pos[0]
            y += word_height
            self.rect = pg.Rect(self.rect[0], self.rect[1], self.rect[2],
                                word_height * count)

        if self.selected:
            color = st.color_lst[1]
            select_border = pg.Surface(pg.Rect(self.rect).size, pg.SRCALPHA)
            select_border.set_alpha(int(self.alpha))
            pg.draw.rect(select_border,
                         color,
                         select_border.get_rect(),
                         border_radius=int(self.radius),
                         width=int(st.border_width))
            screen.blit(select_border, self.rect)

    def get_json(self):
        return {
            "name": self.name,
            "fields": {
                "obj": self.__class__.__name__,
                "rect_lst": list(self.rect),
                "text": self.text,
                "color": self.color,
                "font_size": int(self.font_size),
                "font_family": self.font_family,
            },
        }


class Picture(Layer):

    def __init__(self, name, **kwargs):
        self.name = name
        self.file_path = kwargs["file_path"]
        self.image = pg.image.load(self.file_path).convert_alpha()
        self.alpha = int(kwargs.get('alpha', 255))
        self.radius = int(kwargs.get('radius', 0))
        super().__init__(*kwargs["rect_lst"])

    def show(self, screen):
        screen.blit(self.image, self.rect)
        if self.selected:
            color = st.color_lst[1]
            select_border = pg.Surface(pg.Rect(self.rect).size, pg.SRCALPHA)
            select_border.set_alpha(int(self.alpha))
            pg.draw.rect(select_border,
                         color,
                         select_border.get_rect(),
                         border_radius=int(self.radius),
                         width=int(st.border_width))
            screen.blit(select_border, self.rect)

    def get_json(self):
        return {
            "name": self.name,
            "fields": {
                "obj": self.__class__.__name__,
                "file_path": self.file_path,
                "rect_lst": list(self.rect),
            },
        }


class Slide:

    def __init__(self, slide):
        self.width = st.window_width
        self.height = st.window_height
        self.title = st.window_title
        self.window = pg.display.set_mode((self.width, self.height))
        pg.display.set_caption(self.title)
        self.window.fill(st.color_lst[3])
        self.select_obj = None
        self.slide_num = slide
        self.slide_lst = self.load()
        self.name = self.slide_lst[self.slide_num]["name"]
        self.layers = self.convert_to_objects(self.slide_lst)

    @classmethod
    def create_screen(cls, w, h, color):
        new_screen = pg.Surface((w, h))
        new_screen.fill(color, rect=(0, 0, w, h))
        return new_screen

    def load(self, filename=st.filename_json):
        return fj.load_json(st.folder_data, filename)

    def convert_to_objects(self, slide_lst):
        layers = list()
        for obj_dct in slide_lst[self.slide_num]["children"]:
            layout = obj_dct.get("fields", list())
            if layout.get("obj") == "Rectangle":
                rectangle = Rectangle(obj_dct.get("name"), **layout)
                layers.append(rectangle)
            elif layout.get("obj") == "Picture":
                picture = Picture(obj_dct.get("name"), **layout)
                layers.append(picture)
            elif layout.get("obj") == "TextBox":
                textbox = TextBox(obj_dct.get("name"), **layout)
                layers.append(textbox)
        return layers

    def save(self):
        layers_lst = []
        for layer in self.layers:
            layers_lst.append(layer.get_json())
        self.slide_lst[self.slide_num] = {
            "name": self.name,
            "children": layers_lst
        }
        fj.save_json(st.folder_data, st.filename_json, self.slide_lst)


def main(prsd):
    prsd.window.fill(st.color_lst[5])
    for layer in prsd.layers:
        layer.show(prsd.window)

    running = True
    while running:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False

            elif event.type == pg.MOUSEBUTTONDOWN and event.button == 1:
                prsd.select_obj = None
                for bl in prsd.layers[::-1]:
                    prsd.select_obj = bl.is_selected(event.pos)
                    if prsd.select_obj:
                        break
                if prsd.select_obj:
                    for layer in prsd.layers:
                        if prsd.select_obj is layer:
                            layer.select()
                        else:
                            layer.deselect()
                        layer.show(prsd.window)

            elif event.type == pg.MOUSEBUTTONUP and event.button == 1:
                for layer in prsd.layers:
                    layer.deselect()
                    layer.show(prsd.window)
                prsd.select_obj = None

            elif event.type == pg.MOUSEMOTION:
                if prsd.select_obj:
                    prsd.select_obj.rect.move_ip(event.rel)
                    prsd.select_obj.show(prsd.window)

        prsd.window.fill(st.color_lst[4])
        for layer in prsd.layers:
            layer.show(prsd.window)
        pg.display.flip()


if __name__ == "__main__":
    pg.init()
    prsd = Slide(slide=11)
    main(prsd)
    prsd.save()
    pg.quit()
