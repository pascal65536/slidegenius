from compose import Slide
import settings as st
import pygame as pg
import datetime
import img2pdf
import os



if __name__ == "__main__":
    folder_name = "output"
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
        
    pg.init()
    tmp = Slide(slide=0)
    img_lst = list()
    for slide in range(len(tmp.slide_lst)):
        prsd = Slide(slide=slide)
        prsd.window.fill(st.color_lst[0])
        for layer in prsd.layers:
            layer.show(prsd.window)
        pg.display.flip()
        pg.image.save(prsd.window, f"output/image{len(tmp.slide_lst)-slide}.png")
        img_lst.append(f"output/image{len(tmp.slide_lst)-slide}.png")
    pg.quit()

    img_lst = img_lst[::-1]

    today = datetime.datetime.now()
    file_name = f"result_{today.year}_{today.month:02}_{today.day:02}_{today.hour:02}_{today.minute:02}.pdf"
    file_pdf = os.path.join(folder_name, file_name)
    with open(file_pdf, "wb") as f:
        f.write(img2pdf.convert(img_lst))
    print(file_pdf)