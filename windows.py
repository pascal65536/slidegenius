import sys
import os
import sys
import shutil
import settings as st
import presentation as prs
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (
    QAction,
    QComboBox,
    QFileDialog,
    QGridLayout,
    QHBoxLayout,
    QInputDialog,
    QLabel,
    QLineEdit,
    QMainWindow,
    QMenu,
    QMessageBox,
    QPushButton,
    QSpinBox,
    QVBoxLayout,
    QWidget,
    QApplication,
    QMainWindow,
    QTreeView,
    QMenu,
    QAction,
    QInputDialog,
    QMessageBox,
)

rect_dct = {
    0: {"label": "TopX:", "coords_label": (0, 0), "coords_spinbox": (0, 1)},
    1: {"label": "TopY:", "coords_label": (1, 0), "coords_spinbox": (1, 1)},
    2: {"label": "Width:", "coords_label": (0, 2), "coords_spinbox": (0, 3)},
    3: {"label": "Height:", "coords_label": (1, 2), "coords_spinbox": (1, 3)},
}


class LayerEditWidget(QWidget):

    def __init__(self, parent=None):
        super(LayerEditWidget, self).__init__(parent)
        self.slide_num = None
        self.layout_num = None
        self.name_str = None
        self.name_bak = None
        self.fields_dct = dict()
        self.fields_bak = dict()

        presentation_layout = QHBoxLayout()
        self.slide_lineedit = QLineEdit()
        self.slide_lineedit.setDisabled(True)
        self.slide_lineedit.setFixedWidth(50)
        self.layout_lineedit = QLineEdit()
        self.layout_lineedit.setDisabled(True)
        self.layout_lineedit.setFixedWidth(50)
        self.name_label = QLabel("Name:")
        self.name_lineedit = QLineEdit()
        presentation_layout.addWidget(self.slide_lineedit)
        presentation_layout.addWidget(self.layout_lineedit)
        presentation_layout.addWidget(self.name_label)
        presentation_layout.addWidget(self.name_lineedit)

        self.obj_label = QLabel("Object:")
        self.obj_combobox = QComboBox()
        self.obj_combobox.addItems(st.obj_lst)

        self.rect_label = QLabel("Rect:")
        self.rect_lst = list()
        for _ in range(4):
            rect_spinbox = QSpinBox()
            rect_spinbox.setMinimum(-100)
            rect_spinbox.setMaximum(1600)
            self.rect_lst.append(rect_spinbox)

        self.text_label = QLabel("Text:")
        self.text_lineedit = QLineEdit()

        self.color_label = QLabel("Color:")
        self.color_combobox = QComboBox()
        self.color_combobox.addItems(st.color_lst)

        self.alpha_label = QLabel("Alpha:")
        self.alpha_spinbox = QSpinBox()
        self.alpha_spinbox.setMinimum(0)
        self.alpha_spinbox.setMaximum(255)
        
        self.radius_label = QLabel("Radius:")
        self.radius_spinbox = QSpinBox()
        self.radius_spinbox.setMinimum(0)
        self.radius_spinbox.setMaximum(60)

        self.bordercolor_label = QLabel("BorderColor:")
        self.bordercolor_combobox = QComboBox()
        self.bordercolor_combobox.addItems(st.color_lst)

        self.borderwidth_label = QLabel("BorderWidth:")
        self.borderwidth_combobox = QComboBox()
        self.borderwidth_combobox.addItems(list(map(str, st.borderwidth_lst)))

        self.file_path_label = QLabel("File Path:")
        self.file_path_lineedit = QLineEdit()
        self.file_path_button = QPushButton("Browse")
        self.file_path_button.clicked.connect(self.browse_file)

        self.font_family_label = QLabel("Font Family:")
        self.font_family_combobox = QComboBox()
        self.font_family_combobox.addItems(list(map(str, st.font_family_lst)))

        self.font_size_label = QLabel("Font Size:")
        self.font_size_combobox = QComboBox()
        self.font_size_combobox.addItems(list(map(str, st.font_size_lst)))

        self.save_button = QPushButton("Save")
        self.save_button.clicked.connect(self.save_to_layout)
        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.reset)

        rect_layout = QGridLayout()
        rect_layout.setSpacing(10)
        for num, rect_spinbox in enumerate(self.rect_lst):
            rect_label = QLabel(rect_dct[num]["label"])
            rect_layout.addWidget(rect_label, *rect_dct[num]["coords_label"])
            rect_layout.addWidget(rect_spinbox, *rect_dct[num]["coords_spinbox"])

        layout = QVBoxLayout()
        layout.addWidget(self.obj_label)
        layout.addWidget(self.obj_combobox)
        layout.addLayout(presentation_layout)

        layout.addWidget(self.rect_label)
        layout.addLayout(rect_layout)

        layout.addWidget(self.text_label)
        layout.addWidget(self.text_lineedit)

        color_layout = QHBoxLayout()
        color_layout.addWidget(self.color_label)
        color_layout.addWidget(self.color_combobox)
        color_layout.addWidget(self.alpha_label)
        color_layout.addWidget(self.alpha_spinbox)
        layout.addLayout(color_layout)

        border_layout = QHBoxLayout()
        border_layout.addWidget(self.bordercolor_label)
        border_layout.addWidget(self.bordercolor_combobox)
        border_layout.addWidget(self.borderwidth_label)
        border_layout.addWidget(self.borderwidth_combobox)
        layout.addLayout(border_layout)
        
        radius_layout = QHBoxLayout()
        radius_layout.addWidget(self.radius_label)
        radius_layout.addWidget(self.radius_spinbox)
        layout.addLayout(radius_layout)

        file_layout = QHBoxLayout()
        file_layout.addWidget(self.file_path_label)   
        file_layout.addWidget(self.file_path_lineedit)
        file_layout.addWidget(self.file_path_button)
        layout.addLayout(file_layout)
        
        layout.addWidget(self.font_family_label)
        layout.addWidget(self.font_family_combobox)
        layout.addWidget(self.font_size_label)
        layout.addWidget(self.font_size_combobox)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.save_button)
        button_layout.addWidget(self.cancel_button)
        layout.addLayout(button_layout)

        self.setLayout(layout)

    def save_to_layout(self):
        self.slide_num = int(self.slide_lineedit.text())
        self.layout_num = int(self.layout_lineedit.text())
        self.name_str = self.name_lineedit.text()
        rect_lst = list()
        for rect_spinbox in self.rect_lst:
            rect_lst.append(rect_spinbox.value())
        fields = {
            "text": self.text_lineedit.text(),
            "file_path": self.file_path_lineedit.text(),
            "rect_lst": rect_lst,
            "color": self.color_combobox.currentText(),
            "alpha": self.alpha_spinbox.value(),
            "radius": self.radius_spinbox.value(),
            "bordercolor": self.bordercolor_combobox.currentText(),
            "borderwidth": int(self.borderwidth_combobox.currentText()),
            "font_size": int(self.font_size_combobox.currentText()),
            "font_family": self.font_family_combobox.currentText(),
            "obj": self.obj_combobox.currentText(),
        }

        parent = self.parentWidget().parent()
        parent.presentation.update_layout(
            self.slide_num, self.layout_num, name=self.name_str, fields=fields
        )
        parent.presentation.save()
        parent.model.clear()
        parent.tree_view.show()
        parent.update_tree()
        self.show()

    def browse_file(self):
        folder_name = st.folder_data
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)
        file_path, _ = QFileDialog.getOpenFileName(self, "Select File")
        if not file_path:
            return
        filename = file_path.split("/")[-1]
        dst = os.path.join(folder_name, filename)
        self.file_path_lineedit.setText(dst)
        shutil.copyfile(file_path, dst)

    def reset(self):
        self.name_str = self.name_bak
        self.fields_dct = self.fields_bak
        self.show()

    def reseive_data(self, slide, layout, name, data):
        self.slide_num = slide
        self.layout_num = layout
        self.name_str = name
        self.name_bak = name
        self.fields_dct = data["fields"]
        self.fields_bak = data["fields"]
        for key, values in st.default_fields.items():
            kv = data["fields"].get(key, values)
            self.fields_dct.setdefault(key, kv)
        self.show()

    def show(self):
        self.slide_lineedit.setText(str(self.slide_num))
        self.layout_lineedit.setText(str(self.layout_num))
        self.name_lineedit.setText(self.name_str)
        self.text_lineedit.setText(self.fields_dct.get("text"))
        self.file_path_lineedit.setText(self.fields_dct.get("file_path"))
        self.alpha_spinbox.setValue(self.fields_dct.get("alpha", 255))
        self.radius_spinbox.setValue(self.fields_dct.get("radius", 0))
        self.obj_combobox.setCurrentIndex(
            st.obj_lst.index(self.fields_dct.get("obj", 0))
        )
        self.color_combobox.setCurrentIndex(
            st.color_lst.index(self.fields_dct.get("color", 0))
        )
        self.bordercolor_combobox.setCurrentIndex(
            st.color_lst.index(self.fields_dct.get("bordercolor", 0))
        )
        self.borderwidth_combobox.setCurrentIndex(
            st.borderwidth_lst.index(self.fields_dct.get("borderwidth", 0))
        )
        self.font_size_combobox.setCurrentIndex(
            st.font_size_lst.index(self.fields_dct.get("font_size", 18))
        )
        rect_lst = self.fields_dct.get("rect_lst", [0, 0, 100, 100])
        for index, rect_spinbox in enumerate(self.rect_lst):
            rect_spinbox.setValue(rect_lst[index])


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("Slide Genius")
        self.resize(800, 600)

        self.tree_view = QTreeView(self)
        self.model = QStandardItemModel()
        self.tree_view.setModel(self.model)

        self.layer_widget = LayerEditWidget()

        self.main_layout = QHBoxLayout()
        self.main_layout.addWidget(self.tree_view)
        self.main_layout.addWidget(self.layer_widget)

        self.slide_widget = QWidget()
        self.setCentralWidget(self.slide_widget)
        self.slide_widget.setLayout(self.main_layout)

        self.presentation = prs.Presentation()
        self.update_tree()

        self.tree_view.clicked.connect(self.show_detail)
        self.model.dataChanged.connect(self.handle_data_changed)
        self.tree_view.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tree_view.customContextMenuRequested.connect(self.show_context_menu)

    def update_tree(self):
        for num in range(self.presentation.get_len()):
            slide = self.presentation.get_slide(num)
            slide_name = slide["name"]
            slide_layout = slide.get("children", list())
            slide_item = QStandardItem(slide_name)
            self.model.appendRow(slide_item)
            for layer in slide_layout:
                layer_name = layer["name"]
                layer_item = QStandardItem(layer_name)
                slide_item.appendRow(layer_item)

    def show_context_menu(self, position):
        index = self.tree_view.indexAt(position)
        if index.isValid():
            item = self.model.itemFromIndex(index)
            menu = QMenu(self)

            add_child_action = QAction("Добавить потомка", self)
            add_child_action.triggered.connect(lambda: self.add_child(item))
            menu.addAction(add_child_action)

            add_sibling_action = QAction("Добавить узел на этом уровне", self)
            add_sibling_action.triggered.connect(lambda: self.add_sibling(item))
            menu.addAction(add_sibling_action)

            edit_action = QAction("Редактировать", self)
            edit_action.triggered.connect(lambda: self.edit_item(item))
            menu.addAction(edit_action)

            delete_action = QAction("Удалить", self)
            delete_action.triggered.connect(lambda: self.delete_item(item))
            menu.addAction(delete_action)

            menu.exec_(self.tree_view.viewport().mapToGlobal(position))

    def handle_data_changed(self, index):
        parent = index.parent().row()
        name = index.data(Qt.EditRole)
        if parent >= 0:
            self.presentation.update_layout(parent, index.row(), name=name)
        else:
            self.presentation.update_slide(index.row(), name=name)
        self.presentation.save()
        self.model.clear()
        self.tree_view.show()
        self.update_tree()

    def edit_item(self, item):
        name, ok = QInputDialog.getText(self, "Редактировать", "Введите новое имя:")
        if ok:
            parent = item.parent()
            if parent:
                self.presentation.update_layout(parent.row(), item.row(), name=name)
            else:
                self.presentation.update_slide(item.row(), name=name)

            self.presentation.save()
            self.model.clear()
            self.tree_view.show()
            self.update_tree()

    def add_sibling(self, item):
        parent = item.parent()
        if parent:
            name, ok = QInputDialog.getText(self, "Добавить слой", "Введите имя слоя:")
            if ok:
                self.presentation.append_layout(parent.row(), name=name)
        else:
            name, ok = QInputDialog.getText(
                self, "Добавить слайд", "Введите имя слайда:"
            )
            if ok:
                self.presentation.append_slide(name=name)

        self.presentation.save()
        self.model.clear()
        self.tree_view.show()
        self.update_tree()

    def add_child(self, item):
        parent = item.parent()
        if parent:
            QMessageBox.warning(
                self,
                "У слоя не может быть потомка",
                "Слой - это самый глубокий уровень вложенности. Вся магия происходит на нём.",
                QMessageBox.Ok,
            )
            return
        else:
            name, ok = QInputDialog.getText(self, "Добавить слой", "Введите имя слоя:")
            if ok:
                self.presentation.append_layout(item.row(), name=name)

        self.presentation.save()
        self.model.clear()
        self.tree_view.show()
        self.update_tree()

    def delete_item(self, item):
        parent = item.parent()
        if parent:
            self.presentation.delete_layout(parent.row(), item.row())
        else:
            force = False
            if self.presentation.get_len_slide(item.row()):
                reply = QMessageBox.question(
                    self,
                    "Удаление слайда",
                    "Слайд содержит слои. Удалить все слои вместе со слайдом?",
                    QMessageBox.Yes | QMessageBox.No,
                )
                if reply == QMessageBox.Yes:
                    force = True
                else:
                    return
            self.presentation.delete_slide(item.row(), force=force)

        self.presentation.save()
        self.model.clear()
        self.tree_view.show()
        self.update_tree()

    def show_detail(self, index):
        item = self.model.itemFromIndex(index)
        parent = item.parent()
        if parent:
            detail_dct = {
                "slide": parent.row(),
                "layout": item.row(),
                "name": item.text(),
                "data": self.presentation.get_layout(parent.row(), item.row()),
            }
            self.layer_widget.reseive_data(**detail_dct)
        else:
            print(
                f'Выбран слайд {item.row()}, который называется "{item.text()}".',
                "Детальный просмотр недоступен",
            )
            import compose, pygame as pg

            pg.init()
            prsd = compose.Slide(slide=item.row())
            compose.main(prsd)
            prsd.save()
            pg.quit()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
