import os

window_width = 1600
window_height = 900
window_title = "Figures & Pixels"
border_width = 5
filename_json = "layers.json"
folder_data = "data"
obj_lst = ["TextBox", "Picture", "Rectangle"]
color_lst = ["BLACK", "RED", "GREEN", "BLUE", "WHITE", "GRAY", "GRAY12", "springgreen4", "darkseagreen3", "cornsilk3", "goldenrod", "lightgoldenrod3", "red4", "red3", "DARKORANGE", "darkslateblue"]
# font_size_lst = [8, 9, 10, 12, 14, 16, 18, 20, 36]
font_size_lst = list(map(int, range(10, 72, 2)))
borderwidth_lst = list(map(int, range(10)))
default_fields = {
    "text": None,
    "file_path": None,
    "rect_lst": [0, 0, 100, 100],
    "color": color_lst[3],
    "font_size": font_size_lst[6],
    "obj": obj_lst[0],
    "borderwidth": borderwidth_lst[0],
    "bordercolor": color_lst[0],
}
default_tree_lst = [{"name": "Slide1", "children": [{"name": "Layer1", "fields": {}}]}]

folder_fonts = "fonts"
font_family_lst = list()
for address, dirs, files in os.walk(folder_fonts):
    for fl in files:
        if not (".otf" in fl.lower() or ".ttf" in fl.lower()):
            continue
        os.path.join(folder_fonts, fl)
        font_family_lst.append(os.path.join(folder_fonts, fl))
