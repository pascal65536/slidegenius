import settings as st
import file_json as fj


class Presentation:
    def __init__(self):
        self.tree_lst = fj.load_json(st.folder_data, st.filename_json)
        if not self.tree_lst:
            self.tree_lst = st.default_tree_lst
        self.obj_lst = st.obj_lst
        self.color_lst = st.color_lst
        self.font_size_lst = st.font_size_lst
        self.font_family_lst = st.font_family_lst

    def clear(self):
        self.tree_lst.clear()

    def get_len(self):
        return len(self.tree_lst)

    def save(self):
        fj.save_json(st.folder_data, st.filename_json, self.tree_lst)

    def append_slide(self, name):
        self.tree_lst.append({"name": name, "children": list()})

    def get_len_slide(self, num):
        return len(self.tree_lst[num]["children"])

    def delete_slide(self, num, force=True):
        if not force:
            assert self.get_len_slide(num) == 0, "Не пустой"
        return self.tree_lst.pop(num)

    def clear_slide(self, num):
        self.delete_slide(num, force=True)

    def get_slide(self, num):
        return self.tree_lst[num]

    def update_slide(self, num, **kwargs):
        self.tree_lst[num] = {
            "name": kwargs.get("name", self.tree_lst[num]["name"]),
            "children": kwargs.get("children", self.tree_lst[num]["children"]),
        }

    def show_slide(self, num=None):
        if num:
            print(self.tree_lst[num])
        else:
            print(self.tree_lst)

    def append_layout(self, slide, **kwargs):
        name = kwargs.get("name")
        fields = kwargs.get("fields", dict())
        if name:
            self.tree_lst[slide]["children"].append({
                "name": name,
                "fields": fields
            })

    def get_layout(self, slide, num):
        return self.tree_lst[slide]["children"][num]

    def delete_layout(self, slide, num):
        return self.tree_lst[slide]["children"].pop(num)

    def update_layout(self, slide, num, **kwargs):
        if len(self.tree_lst[slide]["children"]) <= num:
            self.append_layout(slide, **kwargs)
        else:
            self.tree_lst[slide]["children"][num] = {
                "name":
                kwargs.get("name",
                           self.tree_lst[slide]["children"][num]["name"]),
                "fields":
                kwargs.get("fields",
                           self.tree_lst[slide]["children"][num]["fields"]),
            }


if __name__ == "__main__":
    pr = Presentation()
    pr.clear()
    pr.append_slide("Bob")
    pr.update_layout(0, 1, name="Om Nom Nom")
    pr.save()

    pr.append_layout(0, name="dog")
    pr.append_layout(0, name="cat")
    pr.append_layout(0, name="fish")
    pr.append_layout(0, name="frog")
    pr.save()
    print(pr.delete_layout(0, 3))
    pr.save()
    pr.update_slide(0, name="Test")
    pr.save()
    children = [{"name": "Layer1", "fields": {}}]
    pr.update_slide(0, children=children)
    pr.save()
    pr.update_slide(0, name="Mom")
    pr.save()
    for num in range(pr.get_len()):
        pr.show_slide(num)
    # print(pr.delete_slide(0, force=False))

    pr.append_slide("Dad")
    pr.append_slide("Son")
    pr.append_layout(2, name="dog")
    pr.append_layout(2, name="cat")
    pr.append_layout(2, name="fish")
    pr.append_layout(2, name="frog")
    pr.save()

    pr.update_layout(0, 1, name="Om Nom Nom")
    pr.save()
